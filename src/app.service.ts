import { Injectable } from '@nestjs/common';
import { Menu } from './interfaces/menu.interface';
import { User } from './interfaces/user.interface';

const dummyData: Menu[] = [
  {
    id: 1,
    title: 'Paket Nasi + Ayam Bakar',
    description: 'ayam bakar khas daerah xyz',
    items: [
      {
        label: 'Nasi putih',
        icon: 'rice',
        disabled: false,
      },
      {
        label: 'Ayam Bakar',
        icon: 'meat',
        disabled: false,
      },
    ],
    price: 21000,
  },
  {
    id: 2,
    title: 'Paket Nasi + Ikan bakar',
    description: 'ikan bakar khas daerah xyz',
    items: [
      {
        label: 'Nasi putih',
        icon: 'rice',
        disabled: false,
      },
      {
        label: 'Ikan bakar',
        icon: 'fish',
        disabled: false,
      },
    ],
    price: 32000,
  },
];

const dummyUser: User[] = [
  {
    id: 1,
    nama: 'John Doe',
    email: '3K3e3@example.com',
    umur: 30,
    tanggal_lahir: '1990-01-01',
    status: true,
  },
];

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  getMenus(): Menu[] {
    return dummyData;
  }

  createMenu(menu: Menu): Menu {
    dummyData.push(menu);
    return menu;
  }

  updateMenu(id: number, updateMenu: Partial<Menu>): Menu | string {
    const index = dummyData.findIndex((menu) => menu.id === Number(id));
    if (index !== -1) {
      dummyData[index] = { ...dummyData[index], ...updateMenu };
      return dummyData[index];
    } else {
      return 'Gagal melakukan update atau id tidak ditemukan';
    }
  }

  deleteMenu(id: number): Menu | string {
    const index = dummyData.findIndex((menu) => menu.id === Number(id));
    if (index !== -1) {
      const result: Menu = dummyData[index];
      dummyData.splice(index, 1);
      return result;
    } else {
      return 'Gagal menghapus atau id tidak ditemukan';
    }
  }

  getUsers(): User[] {
    return dummyUser;
  }

  createUser(user: User): User {
    dummyUser.push(user);
    return user;
  }

  updateUser(id: number, updateUser: Partial<User>): User | string {
    const index = dummyUser.findIndex((user) => user.id === Number(id));
    if (index !== -1) {
      dummyUser[index] = { ...dummyUser[index], ...updateUser };
      return dummyUser[index];
    } else {
      return 'Gagal melakukan update atau id tidak ditemukan';
    }
  }

  deleteUser(id: number): User | string {
    const index = dummyUser.findIndex((user) => user.id === Number(id));
    if (index !== -1) {
      const result: User = dummyUser[index];
      dummyUser.splice(index, 1);
      return result;
    } else {
      return 'Gagal menghapus atau id tidak ditemukan';
    }
  }
}
