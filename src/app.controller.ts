import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
} from '@nestjs/common';
import { AppService } from './app.service';
import { Menu } from './interfaces/menu.interface';
import { User } from './interfaces/user.interface';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('menus')
  getMenus(): Menu[] {
    return this.appService.getMenus();
  }

  @Post('menus')
  createMenu(@Body() menu: Menu): Menu {
    menu.id = Math.floor(Math.random() * 1000) + 1;
    return this.appService.createMenu(menu);
  }

  @Put('menus/:id')
  updateMenu(
    @Param('id') id: number,
    @Body() updateMenu: Partial<Menu>,
  ): Menu | string {
    return this.appService.updateMenu(id, updateMenu);
  }

  @Delete('menus/:id')
  deleteMenu(@Param('id') id: number): Menu | string {
    return this.appService.deleteMenu(id);
  }

  @Get('users')
  getUsers(): User[] {
    return this.appService.getUsers();
  }

  @Post('users')
  createUser(@Body() user: User): User {
    user.id = Math.floor(Math.random() * 1000) + 1;
    return this.appService.createUser(user);
  }

  @Put('users/:id')
  updateUser(
    @Param('id') id: number,
    @Body() updateUser: Partial<User>,
  ): User | string {
    return this.appService.updateUser(id, updateUser);
  }

  @Delete('users/:id')
  deleteUser(@Param('id') id: number): User | string {
    return this.appService.deleteUser(id);
  }
}
